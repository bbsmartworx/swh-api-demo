import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/pages/Login'
import Devices from '@/pages/Devices'
import Status from '@/pages/Status'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/devices',
      name: 'devices',
      component: Devices
    },
    {
      path: '/devices/:guid',
      name: 'status',
      component: Status
    }
  ]
})
