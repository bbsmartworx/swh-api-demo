export default {
  devicesUrl: 'https://hub.bb-smartworx.com/api/v1.0/hub/devices',
  statusUrl: 'https://hub.bb-smartworx.com/api/v1.0/hub/devices/status/',
  loginUrl: 'https://hub.bb-smartworx.com/api/v1.0/hub/login',
  token: '',
  lastError: ''
}
