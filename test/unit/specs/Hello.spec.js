import Vue from 'vue'
import Hello from '@/pages/Login'

describe('login.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(Hello)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('.md-button').textContent)
      .to.equal('Login')
  })
})
